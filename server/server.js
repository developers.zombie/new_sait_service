const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const http = require("http");
const socketIo = require("socket.io");
const app = express();
const server = http.createServer(app);
const io = socketIo(server);
const { port, environment } = require('../config');
const { 
 getCountNotificacion,
 getIncidentes, 
 getMensajes, 
 getCountObservacion 
} = require('../src/services/');


//socket funciones para realtime.....
io.on("connection", async (socket) => {


    let incidencias = await getIncidentes();

    let mensajes = await getMensajes();

    let countNotificacion = await getCountNotificacion();

    let countObservacion = await getCountObservacion();

    
    io.sockets.emit("incidencias", incidencias.rows);

    socket.emit("mensajes", mensajes.rows);

    socket.emit('countNotificacion' , countNotificacion.rows);

    socket.emit('countObservacion' , countObservacion.rows);
       

    socket.on('add_action', function (data) {

        console.log(data)
        switch (data) {
            case 'insert':

                console.log('new insert');

                io.sockets.emit("incidencias", incidencias.rows);

                break;

            case 'update':
                console.log('update now');

                io.sockets.emit('countObservacion' , countObservacion.rows);

                break;

            default:

                io.sockets.emit("incidencias", incidencias.rows);

                break;
        }
    });

});


//Seguridad en express
app.use(helmet());

//soporte para cors
app.use(cors({
    origin: '*',
    optionsSuccessStatus: 200
}));

app.use(express.json({
    limit: "300kb"
}));


app.use(function (req, res, next) {
    if (req.originalUrl && req.originalUrl.split("/").pop() === 'favicon.ico') {
        return res.sendStatus(204);
    }
    return next();
});



if (environment == 'test') {
    app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
}

module.exports = server;

