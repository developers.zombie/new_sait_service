const client = require('../db/conexion');

client.connect();

module.exports = {

  getIncidentes: async () => {

    try {

      const sql = `SELECT categorias.nombre as categoria, motivos.id as mid,
                motivos.nombre as motivo, motivos.categoria_id,
                incidencias.id as iid, incidencias.asignada,
                incidencias.verificada, incidencias.user_id_asignado,
                incidencias.hora, incidencias.fecha, incidencias.estado_id,
                incidencias.codigo, personas.p_nombre, personas.p_apellido,
                users.id as uid, users.tipo, departamentos.nombre as departamento
                FROM incidencias
                JOIN motivos ON(motivos.id = incidencias.motivo_id)
                JOIN categorias ON(categorias.id = motivos.categoria_id)
                JOIN users ON(users.id = incidencias.user_id)
                JOIN personas ON(personas.id = users.persona_id)
                JOIN departamentos ON(departamentos.id = users.departamento_id)
                WHERE incidencias.asignada = 'false'
                ORDER BY incidencias.id DESC`;

      let incidencias = await client.query(sql);

      // client.end();

      return incidencias;

    } catch (error) {

      client.end();
    }

  },

  getMensajes: async () => {
    try {

      const sql = `SELECT * FROM observaciones
      JOIN incidencias ON (incidencias.id = observaciones.incidencia_id )
      WHERE incidencias.verificada  = false
      AND observaciones.visto  = false`;

      let countM = await client.query(sql);

      // client.end();

      return countM;

    } catch (error) {

      client.end();
    }
  },

  getNotificacion: async () => {
    try {

      const sql = `SELECT * FROM observaciones
      JOIN incidencias ON (incidencias.id = observaciones.incidencia_id )
      WHERE incidencias.verificada  = 'false'
      WHERE observaciones.visto  = 'false`;

      let countM = await client.query(sql) || [];

      // client.end();

      return countM;

    } catch (error) {

      client.end();
    }
  },

  getCountNotificacion: async () => {
    try {

      const sql = `SELECT COUNT(*) FROM notificacions WHERE notificacions.visto  = 'false'`;

      let count = await client.query(sql) || 0;
      return count;

    } catch (error) {

      client.end();
    }
  },
  
  //mensajes
  getCountObservacion: async () => {
    try {

       const sql = `SELECT COUNT(*) FROM observaciones 
       JOIN incidencias ON(incidencias.id = observaciones.incidencia_id)
       WHERE observaciones.visto  = 'false'
       AND incidencias.verificada  = 'false'`;

      let count = await client.query(sql) || 0;
      return count;

    } catch (error) {

      client.end();
    }
  }


}


