//conexion db


const { Client } = require('pg');

const { dbname, dbuser, dbpass, dbport, dbhost } = require('../../config/');

const db_con = new Client({
    user: dbuser,
    host: dbhost,
    database: dbname,
    password: dbpass,
    port: dbport,
});

module.exports = db_con;

