//CONFIGURACION DEL APP
require("dotenv").config();


const config = {
    environment: process.env.NODE_ENV,
    port: process.env.PORT || 3000,
    dbname: process.env.DB_DATABASE,
    dbuser: process.env.DB_USERNAME,
    dbhost: process.env.DB_HOST,
    dbpass: process.env.DB_PASSWORD,
    dbport: process.env.DB_PORT
};

module.exports = config;
