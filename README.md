# new_sait_service
Servicio para el sistema new-sait websockets

url del repositorio [ver en gitlab ](https://gitlab.com/developers.zombie/new_sait_service.git)

## Dependencias
- nodejs
- socket.io
- pm2 

# Instalación y configuración 
- instalar nodejs [ir a web de nodejs](https://nodejs.org/es/)
- instalar pm2 [ ir al sitio pm2 ](https://pm2.keymetrics.io/)

## Moverse a la carpeta del servicio
```ssh
cd sait_service/
```
## Copiar el archivo .env.example en  .env
```ssh
cp .env.example .env
```
## Configurar las variables de conexión
--- Estas variable es para conectarse a la base de datos del sistema con laravel
```ssh
DB_DATABASE=nombre de la db
DB_USERNAME=usuario
DB_PASSWORD=password
```
## Instalar las dependencias
```ssh
npm install
```
## Inicial el servicio
```ssh
npm run dev
```